
//Pilihan computer
function getPilihanComputer(){
    const comp = Math.random();

    if(comp < 0.34) {
        const batucompKlik = document.querySelector('.batu-comp');
        batucompKlik.style.borderRadius = "8px";
        batucompKlik.style.padding = "10px";
        batucompKlik.style.backgroundColor = '#C4C4C4';
        return 'batu';
    } else if(comp >= 0.34 && comp < 0.67){
        const kertascompKlik = document.querySelector('.kertas-comp');
        kertascompKlik.style.borderRadius = "8px";
        kertascompKlik.style.padding = "10px";
        kertascompKlik.style.backgroundColor = '#C4C4C4';
        return 'kertas';
    } else {
        const guntingcompKlik = document.querySelector('.gunting-comp');
        guntingcompKlik.style.borderRadius = "8px";
        guntingcompKlik.style.padding = "10px";
        guntingcompKlik.style.backgroundColor = '#C4C4C4';
        return 'gunting';
    }

    
}

//fungsi buat aturan permainan

function getHasil(comp, player){
    let result = document.getElementById("image");

    // buat satu kelas per kategori, pake eventlistener
    if( player == comp) {
        result.setAttribute("src", "/assets/img/draw.png");
        return 'SERI';
    } else if(player == 'batu' && comp == 'kertas')  {
        result.setAttribute("src", "/assets/img/com-win.png");
        return 'KALAH';
    } else if(player == 'batu' && comp == 'gunting')  {
        result.setAttribute("src", "/assets/img/player-win.png"); 
        return 'MENANG';
    } else if (player == 'gunting' && comp == 'kertas'){
        result.setAttribute("src", "/assets/img/player-win.png"); 
        return 'MENANG';
    } else if (player == 'gunting' && comp == 'batu'){
        result.setAttribute("src", "/assets/img/com-win.png");
        return 'KALAH';
    } else if (player == 'kertas' && comp == 'batu'){
        result.setAttribute("src", "/assets/img/player-win.png");
        return 'MENANG';
    } else if (player == 'kertas' && comp == 'gunting'){
        result.setAttribute("src","/assets/img/com-win.png");
        return 'KALAH';
    }
}


//Pilihan player,
// fungsi buat batu
const pBatu = document.querySelector('.batu');
pBatu.addEventListener('click', function(){
    const pilihanComputer = getPilihanComputer();
    const pilihanPlayer = pBatu.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);

    console.log('comp : ' + pilihanComputer);
    console.log('player : ' + pilihanPlayer);
    console.log('hasil : ' + hasil);
    
    // document.getElementById("keterangan").innerHTML = hasil;
});

const batuKlik = document.querySelector('.batu');
batuKlik.onclick = ubahWarnaBatu;

function ubahWarnaBatu(){
    batuKlik.style.backgroundColor = '#C4C4C4';
    batuKlik.style.borderRadius = "8px";
    batuKlik.style.padding = "10px";
}

//fungsi buat kertas
const pKertas = document.querySelector('.kertas');
pKertas.addEventListener('click', function(){
    const pilihanComputer = getPilihanComputer();
    const pilihanPlayer = pKertas.className;
    let hasil = getHasil(pilihanComputer, pilihanPlayer);

    console.log('comp : ' + pilihanComputer);
    console.log('player : ' + pilihanPlayer);
    console.log('hasil : ' + hasil);

    // document.getElementById("keterangan").innerHTML = hasil;
});

const kertasKlik = document.querySelector('.kertas');
kertasKlik.onclick = ubahWarnaKertas;

function ubahWarnaKertas(){
    kertasKlik.style.backgroundColor = '#C4C4C4';
    kertasKlik.style.borderRadius = "8px";
    kertasKlik.style.padding = "10px";
}

//fungsi buat gunting
const pGunting = document.querySelector('.gunting');
pGunting.addEventListener('click', function(){
    const pilihanComputer = getPilihanComputer();
    const pilihanPlayer = pGunting.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);

    console.log('comp : ' + pilihanComputer);
    console.log('player : ' + pilihanPlayer);
    console.log('hasil : ' + hasil);

    // document.getElementById("keterangan").innerHTML = hasil;
});

const guntingKlik = document.querySelector('.gunting');
guntingKlik.onclick = ubahWarnaGunting;

function ubahWarnaGunting(){
    guntingKlik.style.backgroundColor = '#C4C4C4';
    guntingKlik.style.borderRadius = "8px";
    guntingKlik.style.padding = "10px";
}

//refresh, pake display none,(pake class)

const playAgain = document.querySelector('.refresh');
    playAgain.addEventListener('click', function(){
        location.reload();
});