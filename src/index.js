const express = require("express");
const app = express();
const morgan = require("morgan");
const registrasi = require("./routers/registrasi");
const router = require("./routers/router");

//middleware third-party
app.use(morgan("common"));

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

/**
 * This is function middleware for error handling
 * @param {*} err 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

//view engine
app.set("view engine", "ejs");
app.set("views", "./src/views");

//config folder public untuk css,img,js 
app.use(express.static('./src/publics/assets'));
//use router
app.use(router);
app.use("/registrasi", registrasi);
app.listen(8000, ()=> {
    console.log("server run on port 8000");
});