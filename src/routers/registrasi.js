const express = require("express");
const registrasi = express.Router();
const users = [];

registrasi.get("/", (req, res) => {
    res.render("registrasi");
})

registrasi.post("/", (req, res) => {
    const {username, password} = req.body;
    const newUser = {
        id: users.length+1,
        username,
        password
    };

    users.push(newUser);

    res.status(201).json({
        "status" : "success",
        "message" : "data user berhasil di simpan",
        "data" : newUser
    });
});

module.exports = registrasi;